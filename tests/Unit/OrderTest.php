<?php

namespace Tests\Unit;

use App\Order;
use App\Product;
use Tests\TestCase;

class OrderTest extends TestCase
{
    /*protected $order;
    protected $product;
    protected $product2;

    public function setUp(): void
    {
        $this->order = new Order;

        $this->product  = new Product('Fallout 4', 59);
        $this->product2 = new Product('Pillowcase', 7);

        $this->order->add($this->product);
        $this->order->add($this->product2);
    }*/

    /** @test */
    function an_order_consists_of_products()
    {
        //$this->assertCount(2, $this->order->products());

        $order = $this->orderWithProducts();

        $this->assertCount(2, $order->products());
    }

    /** @test */
    function an_order_can_determine_the_total_cost_of_all_its_products()
    {
        //$this->assertEquals(66, $this->order->total());

        $order = $this->orderWithProducts();

        $this->assertEquals(66, $order->total());
    }

    protected function orderWithProducts()
    {
        $order = new Order();

        $product  = new Product('Fallout 4', 59);
        $product2 = new Product('Pillowcase', 7);

        $order->add($product);
        $order->add($product2);

        return $order;
    }
}

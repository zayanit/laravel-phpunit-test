<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Product;

class ProductTest extends TestCase
{
    protected $product;

    function setUp(): void
    {
        $this->product = new Product('Fallout 4', 59);
    }

    /** @test */
    function a_product_has_name()
    {
        $this->assertEquals('Fallout 4', $this->product->name());
    }

    /** @test */
    function a_product_has_cost()
    {
        $this->assertEquals(59, $this->product->cost());
    }
}
